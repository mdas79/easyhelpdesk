var Template = 'Sites/',
    Instance = require('../Model/Instance'),
    User     = require('../Model/User'),
    Config   = require('../Config/Config'),
    Utils    = require('../Plugins/Utils');

function getRandomInt(min, max) {
  var mathMak = Math.floor(Math.random() * (max - min + 1)) + min;
  return mathMak;
}

function randomString(len) {
  var buf = []
    , chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
    , charlen = chars.length;

  for (var i = 0; i < len; ++i) {
      var randomInt = 'Abcd1234';
      var clen = (charlen - 1);
      randomInt = getRandomInt(0, clen);
      buf.push(chars[randomInt]);
  }

  var randomized = buf.join('');
  return randomized;
}

exports.home = function(req, res){
	//console.log(randomString(7));
  var projectId = Config.projectId;
  if(req.query.projectId) {
    projectId = req.query.projectId;
  }
	res.render(Template + '/home', { 
	        title: 'Chat Sample by Mithun',
	        meta_keyword: 'Login, Sign In',
	        meta_description: 'Chat Sample by Mithun',
	        templateCss: 'dashboard',
	        loggedIn: false,
	        instanceId: randomString(7),
          projectId: projectId
	    });
     
};