/**
Message controller
*/

var Message = require('../Model/Message'),
    Template = 'Messages/';
// Create endpoint /api/users for POST
exports.postMessage = function(req, res) {
    var params = req.body;
    //console.log(params);
    var message = new Message({
        instance: req.body.instance,
        name: req.body.name,
        message: req.body.message
    });

    message.save(function(err) {
        if (err) {
            console.log(err);
            res.end();
        } else {
            res.json({ message: 'Message added to repositary!' });
        }

    });
};

// Create endpoint /api/users for GET
exports.getMessages = function(req, res) {
    var instanceId = req.body.instanceId
    Message.find({ instanceId : instanceId }, function(err, users) {
        if (err) {
            console.log(err);
            res.end();
        } else {
            res.json(users);
        }    
    });
};