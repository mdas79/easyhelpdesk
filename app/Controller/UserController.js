/**
User controller
*/
/**
Module loaders
*/
var User = require('../Model/User'), //Loads user model
    Template = 'Users/'; //Defines User template

/**
Checking the user data for login function from Plugins/Auth
*/
exports.checkLogin = function(param, callback) {
    //Query user table to find data by username
    User.findOne({ username: param.username }, function (err, user) {
        if (err) { 
            callback(err); 
        } else {
            // No user found with that username
            if (!user) { 
                callback(null, false); 
            } else {
                // Make sure the password is correct
                user.verifyPassword(param.password, function(err, isMatch) {
                    if (err) { 
                        callback(false); 
                    } else {
                        // Password did not match
                        if (!isMatch) { 
                            callback(false); 
                        } else {
                            // Success
                            callback(user);
                        }
                    }
                }); 
            }
        }
    });
}

/**
Renders the login page
*/
exports.login = function(req, res){
    res.render(Template + '/login', { 
        title: 'Chat Sample by Mithun',
        meta_keyword: 'Login, Sign In',
        meta_description: 'Chat Sample by Mithun',
        templateCss: 'signin'
    }); 
};
/**
Renders the admin users dashboard
*/
exports.index = function(req, res){
    //checking for logged in users
    var userId = req.session.passport.user;
    var userStatus = false;
    if (userId) {
        User.findOne({ _id: userId }, function(err, user) {
            if (err) {
                console.log(err);
            } else {
                if (user.status == 1) {
                    userStatus = true;
                }
                req.session.username = user.username; 
                res.render(Template + '/index', { 
                    title: 'Admin Panel - Chat Sample by Mithun',
                    meta_keyword: 'Login, Sign In',
                    meta_description: 'Chat Sample by Mithun',
                    templateCss: 'dashboard',
                    loggedIn: true,
                    projectId: user.projectId,
                    userStatus: userStatus,
                    username: user.username
                });
            }
        });
             
    } else { 
        //Or else ask user to login
        res.redirect('/admin/login');
    }
    
};
/**
Logs out the currently logged in user
*/
exports.logout = function(req, res){
    var userId = req.session.passport.user;
    var status = 0;
    User.findOne({ _id: userId }, function(err, user) {
        if(err) {
            console.log(err);
            res.end();
        } else {
            //update user
            user.status = status;
            //save the user
            user.save(function(err) {
                if(err) {
                    console.log(err);
                    res.end();
                } else {
                    console.log(user.status);
                    req.logout();
                    res.redirect('/admin/login');
                }
            });

        }
    });
    
};
/**
Finds a user 
*/
exports.findUser = function(userId, callback) {
    //Query to search for a single user from table by id
    User.findOne({ _id: userId }, function(err, user) {
        if(err) {
            console.log(err);
            callback(false);
        } else {
            callback(user);
        }
    });
}
// Create endpoint /api/users for POST
exports.postUser = function(req, res) {
    var params = req.body;
    //console.log(params);
    //Creates the new User instance
    var user = new User({
        projectId: req.body.projectId,
        username: req.body.username,
        password: req.body.password,
        email: req.body.email
    });
    //Save user
    user.save(function(err) {
        if (err) {
            console.log(err);
            res.end();
        } else {
            res.json({ message: 'User added to repositary!' });
        }

    });
};

// Create endpoint /api/users for GET
exports.getUsers = function(req, res) {
    //Query to fetch all users from db
    User.find(function(err, users) {
        if (err) {
            console.log(err);
            res.end();
        } else {
            res.json(users);
        }    
    });
};

exports.markStatus = function(req, res) {
    var userId = req.session.passport.user;
    console.log(userId);
    var status = req.body.status;
    User.findOne({ _id: userId }, function(err, user) {
        if(err) {
            console.log(err);
            res.end();
        } else {
            //update user
            user.status = status;
            //save the user
            user.save(function(err) {
                if(err) {
                    console.log(err);
                    res.end();
                } else {
                    console.log(user.status);
                    res.json({ message: 'User is updated!' });
                }
            });

        }
    });
}


exports.updateUser = function(req, res) {
    var userId = req.session.passport.user;
    var param = req.body;
    console.log(param);
    //finds the user
    User.findOne({ _id: userId }, function(err, user) {
        if(err) {
            console.log(err);
            res.end();
        } else {
            //update user
            for (prop in req.body) {
                user[prop] = req.body[prop];
            }
            //save the user
            user.save(function(err) {
                if(err) {
                    console.log(err);
                    res.end();
                } else {
                    res.json({ message: 'User is updated!' });
                }
            });

        }
    });
}
// Create endpoint /api/user/:id for DELETE
exports.deleteUser = function(req, res) {
    var userId = req.params.id;
    //remove the user from collection
    User.remove({ _id: userId }, function(err) {
        if(err) {
            console.log(err);
            res.end();
        } else {
            res.json({ message: 'User is deleted!' });
        }
    });
}

exports.onlineAdmins = function(req, res) {
    var projectId = req.body.projectId;
    //Query to fetch all users from db
    User.find({ status: 1, projectId: projectId }, function(err, users) {
        if (err) {
            console.log(err);
            res.end();
        } else {
            res.json(users);
        }    
    });
};