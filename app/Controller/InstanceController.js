/**
User controller
*/

var Instance = require('../Model/Instance'),
    Message    = require('../Model/Message'),
    Utils = require('../Plugins/Utils'),
    Template = 'Instances/';

// Create endpoint /api/users for POST
exports.postInstance = function(req, res) {
    var params = req.body;
    //console.log(params);
    //create new instance
    var instance = new Instance({
        instanceId: req.body.instanceId,
        name: req.body.clientName,
        email: req.body.clientEmail
    }); 
    //save the instance
    instance.save(function(err) {
        if(err) {
            console.log(err);
            res.end();
        } else {
            //start messaging
            var message = new Message({
                instanceId: randomized,
                name: req.body.clientName,
                message: req.body.message
            });

        }
    }) ; 
};

/**
* Get unreplied instances
*/
exports.getUnrepliedInstances = function(req, res) {
    //Find instance which has been not replied
    Instance.find({ replied: 0, projectId : req.query.projectId }, function(err, instances) {
        if (err) {
            console.log(err);
            res.end();
        } else {
            res.json(instances);
        }    
    });
};

/**
* Get connected instances
*/
exports.getConnectedInstances = function(req, res) {
    var username = req.session.username;
    //Find instance which has been not disconnected by admin
    Instance.find({ 
        disconnected: 0, 
        aborted: 0, 
        projectId : req.body.projectId, 
        adminId: { $in: [username, ''] } 
    }, function(err, instances) {
        if (err) {
            console.log(err);
            res.end();
        } else {
            res.json(instances);
        }    
    });
};

/**
* Get disconnected instances
*/
exports.getDisconnectedInstances = function(req, res) {
    var username = req.session.username;
    //Find instance which has been disconnected by admin
    Instance.find({ 
        disconnected: 1, 
        aborted: 0, 
        projectId : req.body.projectId, 
        adminId: { $in: [username, ''] } 
    }, function(err, instances) {
        if (err) {
            console.log(err);
            res.end();
        } else {
            res.json(instances);
        }    
    });
};

/**
* Update a instance
*/
exports.updateInstance = function(req, res) {
    var instanceId = req.params.id;
    var param = req.body;
    //find the instance by id
    Instance.findOne({ _id: instanceId }, function(err, user) {
        if(err) {
            console.log(err);
            res.end();
        } else {
            //update user
            for (prop in req.body) {
                instance[prop] = req.body[prop];
            }
            //save the user
            instance.save(function(err) {
                if(err) {
                    console.log(err);
                    res.end();
                } else {
                    res.json({ message: 'Instance is updated!' });
                }
            });

        }
    });
}

exports.markStatus = function(req, res) {
    var instanceId = req.body.instanceId;
    var status = req.body.status;
    
    Instance.findOne({ instanceId: instanceId }, function(err, instance) {
        if(err) {
            console.log(err);
            res.end();
        } else {
            //update user
            instance.disconnected = status;
            //save the user
            instance.save(function(err) {
                if(err) {
                    console.log(err);
                    res.end();
                } else {
                    console.log(instance.disconnected);
                    res.json({ message: 'Instance is updated!' });
                }
            });

        }
    });
}