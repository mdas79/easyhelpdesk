/**
* Initializing the Router essentials
**/
var express     = require('express'),
	Auth        = require('./Plugins/Auth'),  
	Site        = require('./Controller/SiteController'),
	User        = require('./Controller/UserController'),
	Instance    = require('./Controller/InstanceController'),
	Message     = require('./Controller/MessageController'),
    app         = express();

/** Created an instance of the express Router **/
var router = express.Router(); 


/** Route declaration **/
/** Index route **/

router.post("/users/postLogin", Auth.isAuthenticated, 
	function(req, res) {
        res.redirect('/');
    });



router.get("/", Site.home);

router.get("/admin/login", User.login);
router.get("/admin/logout", User.logout);
router.get("/admin/dashboard", User.index);

router.post("/users/postUser", User.postUser);
router.post("/user/status", User.markStatus);
router.post("/users/listadmins", User.onlineAdmins);

router.post("/instances/listconnected", Instance.getConnectedInstances);
router.post("/instances/listdisconnected", Instance.getDisconnectedInstances);
router.post("/instance/create", Instance.postInstance);
router.post("/instance/status", Instance.markStatus);

router.post("/messages/listall", Message.getMessages);
//router.get("/chat", Site.chat);


/** Expose router to other modules **/
module.exports = router;