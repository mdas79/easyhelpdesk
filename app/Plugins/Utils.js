
exports.randomString = function(len, callback) {
  var buf = []
    , chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
    , charlen = chars.length;

  for (var i = 0; i < len; ++i) {
      var randomInt = 'Abcd1234';
      var clen = (charlen - 1);
      getRandomInt(0, clen, function(rdStr){
          randomInt = rdStr;
      });
    buf.push(chars[randomInt]);
  }

  callback(buf.join(''));
};
