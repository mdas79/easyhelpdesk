/**
* Oauth Authentication system
*/
/**
* Passport loaded with its Strategies
*/
var Passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy;
var User = require('../Controller/UserController');

Passport.serializeUser(function(user, done) {
    done(null, user.id);
});

Passport.deserializeUser(function(id, done) {
    User.findUser(id, function(user){
        //console.log(user);
        done(null, user);
    });
});


Passport.use(new LocalStrategy(function(username, password, done) {
        var param = { "username": username, "password": password };
        User.checkLogin(param, function(user){
            console.log(user);
            if(!user){
                return done(null, false, { message: 'Incorrect username.' });
            }else{
                return done(null, user);
            }            
        });
    }
));





exports.isAuthenticated = Passport.authenticate('local', { successRedirect: '/admin/dashboard', failureRedirect: '/admin/login', failureFlash: false });
