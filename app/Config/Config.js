module.exports = {
    "projectId": '123456',
    "dialect": 'mongodb',
    "host": 'localhost',
    "database": 'chat',
    "hash_method": 'sha256',
    "hash_key": '4b 8?((~FKnpD))>8kb!B |#-uXIO24G3rc:&MG+FR{x;r#Uq4k{Ef@F4E9^-qS!', //change hash key
    "lifetime": 1209600,
    "webroot": '/var/www/html/NodeChat/',
    "routeUrl": 'http://localhost:3000/',
    "security": {
        "tokenLife": 3600
    },
}