/**
* Instance schema
* Copyright(c) 2015 Mithun Das (https://github.com/mithundas79)
* MIT Licensed
*/

var mongoose = require('mongoose'),
    Schema   = mongoose.Schema,
    ObjectId = Schema.ObjectId;


var InstanceSchema = new Schema({
    instanceId			: {
    	type: String, 
    	required: true,
    	index: {
    		unique: true
    	}
    },
    projectId           : {
        type: String,
        required: true
    },
    adminId             : {
        type: String
    },
    name			    : {
    	type: String, 
    	required: true
    },
    email				: {
    	type: String,
    	required: true,
    	trim: true,
    	index: {
    		sparse: true
    	}
    },
    disconnected        : {
        type: String,
        default: 0
    },
    aborted             : {
        type: String,
        default: 0
    },
    replied             : {
        type: String,
        default: 0
    },
    created			    : {
        type: Date,
        default: Date.now
    },
    modified		    : {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('Instance', InstanceSchema);