/**
* Chat schema
* Copyright(c) 2015 Mithun Das (https://github.com/mithundas79)
* MIT Licensed
*/

var mongoose = require('mongoose'),
    Schema   = mongoose.Chat;

var ChatSchema = new Schema({
    userId			: {
    	type: String, 
    	required: true
    },
    message			: {
    	type: String, 
    	required: true
    },
    created			    : {
        type: Date,
        default: Date.now
    },
    modified		    : {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('Chat', ChatSchema);