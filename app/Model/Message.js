/**
* Message schema
* Copyright(c) 2015 Mithun Das (https://github.com/mithundas79)
* MIT Licensed
*/

var mongoose = require('mongoose'),
    Schema   = mongoose.Schema,
    ObjectId = Schema.ObjectId;


var MessageSchema = new Schema({
    instanceId			: {
    	type: String, 
    	required: true
    },
    name			: {
    	type: String, 
    	required: true
    },
    message				: {
    	type: String,
    	required: true,
    	trim: true,
    },
    created			    : {
        type: Date,
        default: Date.now
    },
    modified		    : {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('Message', MessageSchema);