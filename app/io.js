var io = require('socket.io'),
    Instance        = require('./Model/Instance'),
	Message        = require('./Model/Message');

// usernames which are currently connected to the chat
var usernames = {};

io.sockets.on('connection', function (socket) {
	
	// when the client emits 'adduser', this listens and executes
	socket.on('adduser', function(param){
		// store the username in the socket session for this client
		socket.username = param.username;
		// store the room name in the socket session for this client
		socket.room = param.instanceId;
		// add the client's username to the global list
		usernames[username] = param.username;
		// send client to room 1
		socket.join(param.instanceId);
		// echo to client they've connected
		socket.emit('updatechat', 'SERVER', 'you have connected to support chat');
		// echo to room 1 that a person has connected to their room
		socket.broadcast.to(param.instanceId).emit('updatechat', 'SERVER', param.username + ' has connected to this room');
		//socket.emit('updaterooms', rooms, param.instanceId);

		console.log("Is this called?");

		//now save the user here
		var instance = new Instance({
	        instanceId: param.instanceId,
	        name: param.clientName,
	        email: param.clientEmail
	    }); 

	    instance.save(function(err) {
	        if(err) {
	            console.log(err);
	            res.end();
	        }
	    }) ; 

	});
	
	// when the client emits 'sendchat', this listens and executes
	socket.on('sendchat', function (data) {
		// we tell the client to execute 'updatechat' with 2 parameters
		io.sockets.in(socket.room).emit('updatechat', socket.username, data);
		//also save data in db here
	});
	
	// when the user disconnects.. perform this
	socket.on('disconnect', function(){
		// remove the username from global usernames list
		delete usernames[socket.username];
		// update list of users in chat, client-side
		io.sockets.emit('updateusers', usernames);
		// echo globally that this client has left
		socket.broadcast.emit('updatechat', 'SERVER', socket.username + ' has disconnected');
		socket.leave(socket.room);
	});
});

/** Expose router to other modules **/
module.exports = io;