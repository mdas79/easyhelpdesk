/**
* Initializing the Express essentials
**/
var express         = require('express'),
    path            = require('path'),
    favicon         = require('serve-favicon'),
    logger          = require('morgan'),
    cookieParser    = require('cookie-parser'),
    methodOverride  = require('method-override'),
    session         = require('express-session'),
    passport        = require('passport'),
    exphbs          = require('express-handlebars'),
    bodyParser      = require('body-parser'),
    Config          = require('./app/Config/Config');

var app             = express();

var mongoose = require('mongoose');
mongoose.connect(Config.dialect + "://" + Config.host + "/" + Config.database);
//mongoose.connect('mongodb://localhost/chat');

// view engine setup
app.set('views', path.join(__dirname, 'app/View'));
// Register `hbs` as our view engine using its bound `engine()` function.
app.engine('.hbs', exphbs({
    extname: '.hbs',
    defaultLayout: path.join(__dirname, 'app/View/Layouts/default'),
    partialsDir: [
        path.join(__dirname, 'app/View/Elements')
    ]
}));
app.set('view engine', '.hbs');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev')); //combined
app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public')));

app.use(session({ 
    secret: '498f99f3bbee4ae3a075eada02488464',
    saveUninitialized: true,
    resave: true
}));
app.use(passport.initialize());
app.use(passport.session());

/** Router file declaration **/
var router = require('./app/routers');

// Session-persisted message middleware
/*app.use(function(req, res, next){
    var err = req.session.error,
        msg = req.session.notice,
        success = req.session.success;

    delete req.session.error;
    delete req.session.success;
    delete req.session.notice;

    if (err) res.locals.error = err;
    if (msg) res.locals.notice = msg;
    if (success) res.locals.success = success;

    next();
});*/



/** Registration of the route middleware **/
app.use('/', router);



// setup server
app.set('port', process.env.PORT || 3000);
var server = app.listen(app.get('port'), function() {
    console.log('Express server listening on port ' + server.address().port);
});

// Socket.io
var io = require('socket.io').listen(server);


/**** IO server codes ********/

var Instance        = require('./app/Model/Instance'),
    Message        = require('./app/Model/Message');


io.sockets.on('connection', function (socket) {
    console.log('Socket.io user connected');
    // when the client emits 'adduser', this listens and executes
    socket.on('adduser', function(param){
        console.log('Add user emited');
        // store the username in the socket session for this client
        //socket.username = param.username;
        //socket.instanceId = param.instanceId;
        // store the room name in the socket session for this client
        //socket.room = param.instanceId;
        // add the client's username to the global list
        //usernames[param.username] = param.username;
        // send client to room 1
        socket.join(param.instanceId);
        // echo to client they've connected
        //socket.emit('updatechat', 'SERVER', 'you have connected to support chat', 1);
        //socket.emit('updatelist', param);
        // echo to room 1 that a person has connected to their room
        socket.broadcast.to(param.instanceId).emit('updatechat', 'SERVER', param.username + ' has connected to this room', param.instanceId);
        //socket.emit('updaterooms', rooms, param.instanceId);

        //We will check whether the instance is already present
        Instance.findOne({ instanceId: param.instanceId }, function(err, instance) {
            if(err) {
                console.log(err);
            } else {
                if( instance ) {
                    //instance is present, so update instance
                    instance.replied = 1;
                    //save the instance
                    instance.save(function(err) {
                        if(err) {
                            console.log(err);
                        }
                    });    
                } else {
                    //create a new instance
                    var instance = new Instance({
                        instanceId: param.instanceId,
                        name: param.username,
                        email: param.email,
                        projectId: param.projectId,
                        adminId: param.adminId
                    }); 

                    instance.save(function(err) {
                        if(err) {
                            console.log(err);
                        } else {
                            //emit updatelist for admin so that new instance is added to the queue
                            socket.broadcast.emit('updatelist', param);
                        }
                    });
                }
                

            }
        });      

    });
    
    // when the client emits 'sendchat', this listens and executes
    socket.on('sendchat', function (param) {
        // we tell the client to execute 'updatechat' with 2 parameters
        io.sockets.in(param.instanceId).emit('updatechat', param.username, param.message, param.instanceId);
        //console.log(socket.instanceId + socket.username);
        //also save data in db here
        var message = new Message({
            instanceId: param.instanceId,
            name: param.username,
            message: param.message
        });

        message.save(function(err) {
            if (err) {
                console.log(err);
            }
        });
    });


    
    // when the user disconnects.. perform this
    socket.on('removeuser', function(param){
        //console.log(param);
        Instance.findOne({ instanceId: param.instanceId }, function(err, instance) {
            if(err) {
                console.log(err);
            } else {
                //console.log(instance);
                if( instance ) {
                    //update user
                    instance.disconnected = 1;
                    //save the user
                    instance.save(function(err) {
                        if(err) {
                            console.log(err);
                        } else {
                            //socket.emit('updatelist', param); 
                            //console.log(param);  
                            socket.emit('updatebanlist', param);  
                            io.sockets.in(param.instanceId).emit('updatechat', 'SERVER', param.username + ' is disconnected by Admin', param.instanceId);                     
                        }
                    });
                }
            }
        });
    });

    // when the user reconnects.. perform this
    socket.on('reconnectuser', function(param){
        Instance.findOne({ instanceId: param.instanceId }, function(err, instance) {
            if(err) {
                console.log(err);
            } else {
                if( instance ) {
                    //update user
                    instance.disconnected = 0;
                    //save the user
                    instance.save(function(err) {
                        if(err) {
                            console.log(err);
                        } else {
                            console.log(param);
                            socket.emit('updatelist', param);   
                            io.sockets.in(param.instanceId).emit('updatechat', 'SERVER', param.username + ' is reconnected by Admin', param.instanceId);                                         
                        }
                    });   
                }
            }
        });
    });
});

/*** IO codes end ***/

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('Errors/error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('Errors/error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;
